using UnityEngine;

namespace Appside
{
    public interface IBossComponent 
    {
        void InitBattle(Transform laserHit);
        Transform GetTransform();

        void WinBattle();
        void FailBattle();
    }
}
