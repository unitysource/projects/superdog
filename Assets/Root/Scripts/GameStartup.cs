using _Project.Scripts;
using Appside.Components;
using Appside.Level;
using Appside.MonoBehaviours;
using Appside.MonoBehaviours.MeshSlicing;
using Appside.Save;
using Appside.Systems;
using Appside.UI;
using Appside.Utils;
using Cinemachine;
using Leopotam.Ecs;
using Leopotam.Ecs.UnityIntegration;
using LeopotamGroup.Globals;
using UnityEngine;

namespace Appside
{
    public sealed class GameStartup : MonoBehaviour
    {
        [SerializeField] private GameConfig _gameConfig;
        [SerializeField] private EconomConfig _ecomomicaConfig;
        [SerializeField] private SkyboxConfig _skyboxConfig;
        [SerializeField] private DamageConfig _damageConfig;
        [SerializeField] private ShakeConfig _shakeConfig;
        [SerializeField] private CinemachineVirtualCamera _gameCamera;
        [SerializeField] private CinemachineVirtualCamera _battleCamera;
        [SerializeField] private CinemachineVirtualCamera _finishCamera;
        [SerializeField] private SliceController _sliceController;
        [SerializeField] private CameraShaker _cameraShaker;
        private LevelView _levelView;
        private EcsWorld _world = default;
        private EcsSystems _updateSystems = default;
        private ApplicationManager _applicationManager;
        private LevelManager _levelManager;

        void Start()
        {
            AnalyticsManager.FBInitialize();
            AnalyticsManager.GAInitialize();
            AnalyticsManager.GameStarted();
            
            _world = new EcsWorld();
            _updateSystems = new EcsSystems(_world);
#if UNITY_EDITOR
            EcsWorldObserver.Create(_world);
            EcsSystemsObserver.Create(_updateSystems);
#endif
            _applicationManager = new ApplicationManager();

            LoadLevel();

            Service<LevelView>.Set(_levelView);

            _levelManager = new LevelManager(_applicationManager);
            Service<LevelManager>.Set(_levelManager);

            Service<SliceController>.Set(_sliceController);
            _sliceController.enabled = false;

            _updateSystems
                .Add(new InitSystem())
                .Add(new InitViewSystem())
                .Add(new InitEnemySystem())
                .Add(new CameraSystem(_gameCamera, _battleCamera, _finishCamera))
                .Add(new LevelSystem())
                .Add(new UserInputSystem())
                .Add(new MovePlayerSystem())
                .Add(new UpdateStatePlayerSystem())
                .Add(new BattleSystem())
                .Add(new AttackSystem())
                .Add(new LevitationSystem())
                .Add(new HackSystem())
                .Add(new UpdateStateEnemySystem())
                .OneFrame<LevitationEvent>()
                .OneFrame<AttackEvent>()
                .OneFrame<FailedEvent>()
                .OneFrame<CompleteEvent>()
                .OneFrame<WinBattleEvent>()
                .Inject(_gameConfig)
                .Inject(_ecomomicaConfig)
                .Inject(_damageConfig)
                .Inject(_skyboxConfig)
                .Inject(_shakeConfig)
                .Inject(_cameraShaker)
                .Inject(FindObjectOfType<MainMenuView>())
                .Inject(FindObjectOfType<GameView>())
                .Inject(FindObjectOfType<FinishView>())
                .Inject(FindObjectOfType<SettingsView>())
                .Init();
        }

        void Update()
        {
            _updateSystems?.Run();
        }

        void OnDestroy()
        {
            if (_updateSystems != null)
            {
                _updateSystems.Destroy();
                _updateSystems = null;
                _world.Destroy();
                _world = null;
            }
        }

        private void ChangedSkyBox(int currentLevel)
        {
            RenderSettings.skybox = _skyboxConfig.SkyboxTypes[currentLevel - 1].Skybox;
        }

        private void LoadLevel()
        {
            var currentLevel = _applicationManager.PlayerAccount.CurrentLevel;
            var level = "Level_" + currentLevel;
            var levelPrefab = Resources.Load<GameObject>(level);
            _levelView = Instantiate(levelPrefab).GetComponent<LevelView>();
            // ChangedSkyBox(currentLevel);
        }

        private void OnApplicationQuit()
        {
            AnalyticsManager.GameClosed();
        }
    }
}