using Appside.Utils;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Appside.UI
{
    public class HeatScale : MonoBehaviour
    {
        [SerializeField] private Slider _fillImage;
        [SerializeField] private HeatScaleConfig _heatScaleConfig;

        private Coroutine _coroutine;
        private float _indicator;

        public float FillAmount => _fillImage.value;

        public bool CanExplode()
        {
            if (_fillImage.value >= 1.0f)
                return true;
            
            return false;
        }

        public void Levitation()
        {    
             Reset();
            _indicator = 1;
            _coroutine = StartCoroutine(FillScale());
        }

        public void StartAttack()
        {
            Reset();
            _indicator = -1;
            _coroutine = StartCoroutine(FillScale());
        }

        public void Damage(float damage)
        {
            _fillImage.value -= damage;
        }

        public void Reset()
        {
            if (_coroutine != null)
                StopCoroutine(_coroutine);
        }

        IEnumerator FillScale()
        {
            var loop = true;
            while (loop)
            {
                var fill = _fillImage.value + _indicator*Time.deltaTime * _heatScaleConfig.SpeedFill;
                _fillImage.value = Mathf.Clamp(fill, 0, 1);
                if (fill >= 1 || fill <= 0)
                    loop = false;
                
                yield return null;
            }
        }
    }
}
