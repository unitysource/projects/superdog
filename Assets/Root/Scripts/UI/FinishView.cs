using Appside.Level;
using Appside.Utils;
using LeopotamGroup.Globals;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Appside.UI
{
    public class FinishView : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToHide;
        [SerializeField] private Button _reloadButton;
        [SerializeField] private GameObject _successObject;
        [SerializeField] private GameObject _failedGameObject;
        [SerializeField] private TextMeshProUGUI _textReloadButton;
        [SerializeField] private Sprite _successSprite;
        [SerializeField] private Sprite _failedSprite;
        [SerializeField] private TextMeshProUGUI _earnedScoreWin;
        [SerializeField] private TextMeshProUGUI _earnedScoreLose;

        private const string _successText = "NEXT";
        private const string _failedText = "RETRY";
        
        private bool _completed;
        private bool _isShow;
        private int _score;
        
        public void Show(bool completed, int score = 0)
        {
            _reloadButton.onClick.RemoveListener(OnContinueClickButton);
            _reloadButton.onClick.AddListener(OnContinueClickButton);
            _completed = completed;
            _score = score;
            
            SetInfo();
            _objectToHide.SetActive(true);
        }

        public void Hide()
        {
            _objectToHide.SetActive(false);
        }

        private void OnContinueClickButton()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void SetInfo()
        {
            if(_isShow)
                return;
            
            var levelManager = Service<LevelManager>.Get(false);

            if (_completed)
            {
                _successObject.gameObject.SetActive(true);
                _failedGameObject.gameObject.SetActive(false);
                _reloadButton.image.sprite = _successSprite;
                _textReloadButton.text = _successText;
                levelManager.ApplicationManager.PlayerAccount.AddedScore(_score);
                _earnedScoreWin.text = levelManager.ApplicationManager.PlayerAccount.Score.ToString();
                levelManager.ApplicationManager.PlayerAccount.AddedLevel();
            }
            else
            {
                _successObject.gameObject.SetActive(false);
                _failedGameObject.gameObject.SetActive(true);
                _reloadButton.image.sprite = _failedSprite;
                _textReloadButton.text = _failedText;
                _earnedScoreLose.text = levelManager.ApplicationManager.PlayerAccount.Score.ToString();
              
            }

            levelManager.ApplicationManager.SavePlayerAccount();
            _isShow = true;
        }
    }
}
