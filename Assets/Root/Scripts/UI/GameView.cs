using Appside.Level;
using LeopotamGroup.Globals;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Appside.UI
{
    public class GameView : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToHide;
        [SerializeField] private HeatScale _heatScale;
        [SerializeField] private TextMeshProUGUI _currentLevelText;
        [SerializeField] private TextMeshProUGUI _nextLevelText;
        [SerializeField] private TextMeshProUGUI _coin;
        [SerializeField] private Slider _levelSlider;
        public HeatScale HeatScale => _heatScale;
        private LevelManager _levelManager;
        public void Show(int currentLevel)
        {
            _currentLevelText.text = currentLevel.ToString();
            _nextLevelText.text = (currentLevel + 1).ToString();
            _objectToHide.SetActive(true);
            _levelManager = Service<LevelManager>.Get(false);
            _coin.text = _levelManager.ApplicationManager.PlayerAccount.Score.ToString();
        }

        public void Hide()
        {
            _objectToHide.SetActive(false);
        }

        public void StartLevitation()
        {
            _heatScale.Levitation();
        }

        public void StartAttack()
        {
            _heatScale.StartAttack();
        }

        public void Damage(float damage)
        {
            _heatScale.Damage(damage);
        }

        public void StopScale()
        {
            _heatScale.Reset();
        }
        public void UpdateScore()
        {
            if (_levelManager != null)
                _coin.text = _levelManager.ApplicationManager.PlayerAccount.Score.ToString();
        }

        public void UpdateProgressLevel(float progress)
        {
            _levelSlider.value = progress;
        }
    }
}