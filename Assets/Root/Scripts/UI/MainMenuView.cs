using UnityEngine;
using System;
using Appside.Level;
using LeopotamGroup.Globals;
using TMPro;
using UnityEngine.UI;

namespace Appside.UI
{
    public class MainMenuView : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToHide;
        [SerializeField] private GameObject _startGroup;
        [SerializeField] private Button _startButton;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private TextMeshProUGUI _textStatsGold;
        
        public Action StartGameEvent;
        public Action SettingsShowEvent;

        public void Show()
        {
            _objectToHide.SetActive(true);
            var levelManager = Service<LevelManager>.Get(false);
            UpdateScore(levelManager.ApplicationManager.PlayerAccount.Score);
        }
        
        public void Hide()
        {
            _objectToHide.SetActive(false);
            _startButton.onClick.RemoveListener(OnStartButtonClicked);
            _settingsButton.onClick.RemoveListener(OnSettingsButonClicked);
        }

        public void StartGame()
        {
            _startGroup.SetActive(false);
        }

        public void UpdateScore(int countStatsGold)
        {
            _textStatsGold.text = countStatsGold.ToString();
        }

        void Start()
        {
            _startButton.onClick.AddListener(OnStartButtonClicked);
            _settingsButton.onClick.AddListener(OnSettingsButonClicked);
            _objectToHide.SetActive(true);
        }

        void OnStartButtonClicked()
        {
            StartGameEvent?.Invoke();
        }

        void OnSettingsButonClicked()
        {
            SettingsShowEvent?.Invoke();
        }
    }
}
