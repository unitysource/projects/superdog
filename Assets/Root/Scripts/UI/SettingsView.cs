using Appside.Level;
using DG.Tweening;
using LeopotamGroup.Globals;
using UnityEngine.UI;
using UnityEngine;

namespace Appside.UI
{
    public class SettingsView : MonoBehaviour
    {
        [SerializeField] private GameObject _objectToHide;
        [SerializeField] private Button _closeButton;
        [SerializeField] private ToogleController _soundsToogle;
        [SerializeField] private ToogleController _vibroToogle;
        [SerializeField] private Button _vibroButton;
        [SerializeField] private Button _soundsButton;
        [SerializeField] private GameObject _moveObject;

        private LevelManager _levelManager;
        private float _startY;
        private float _duration = 2f;
        private Tween _tween;

        public void Show()
        {
            SetSettings();
            Time.timeScale = 0;
            _objectToHide.SetActive(true);
        //    if (_tween != null)
           //     _tween.Kill();
          //  _tween = _moveObject.GetComponent<RectTransform>().DOAnchorPosY(_startY + 500, _duration);
        }

        public void Hide()
        {
            Time.timeScale = 1;
            _objectToHide.SetActive(false);
         //   if (_tween != null)
         //       _tween.Kill();
           // _tween = _moveObject.GetComponent<RectTransform>().DOAnchorPosY(_startY, _duration);
        }

        void OnSoundButtonClick()
        {
            _levelManager.ApplicationManager.ApplicationSettings.IsSoundOn = !_levelManager.ApplicationManager.ApplicationSettings.IsSoundOn;
            _levelManager.ApplicationManager.SaveApplicationSettings();
        }

        void OnVibrationButtonClick()
        {
            _levelManager.ApplicationManager.ApplicationSettings.IsVibrationOn = !_levelManager.ApplicationManager.ApplicationSettings.IsVibrationOn;
            _levelManager.ApplicationManager.SaveApplicationSettings();
        }

        public void OnEnable()
        {
            _closeButton.onClick.AddListener(Hide);
            _soundsButton.onClick.AddListener(OnSoundButtonClick);
            _vibroButton.onClick.AddListener(OnVibrationButtonClick);
        }

        public void OnDisable()
        {
            _closeButton.onClick.RemoveListener(Hide);
            _soundsButton.onClick.RemoveListener(OnSoundButtonClick);
            _vibroButton.onClick.RemoveListener(OnVibrationButtonClick);
        }

        void SetSettings()
        {
            if (_levelManager == null)
                _levelManager = Service<LevelManager>.Get(false);
            
            _soundsToogle.isOn = _levelManager.ApplicationManager.ApplicationSettings.IsSoundOn;
            _soundsToogle.ShowToogle();
            
            _vibroToogle.isOn = _levelManager.ApplicationManager.ApplicationSettings.IsVibrationOn;
            _vibroToogle.ShowToogle();
        }

        void Start()
        {
            _startY = _moveObject.GetComponent<RectTransform>().localPosition.y;
        }
    }
}
