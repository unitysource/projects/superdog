using UnityEngine;
using UnityEngine.UI;


namespace Appside.UI
{
    public class ToogleController : MonoBehaviour
    {
        public RectTransform toggle;
        public Color onSpriteBg;
        public Color offSpriteBg;

        public GameObject handle;
        private RectTransform handleTransform;

        private float handleSize;
        private float onPosX;
        private float offPosX;

        public float handleOffset;

        public float speed;
        static float t = 0.0f;

        private bool switching = false;
        private Image toggleBgImage;
        public bool isOn;

        private void Constructor()
        {
            handleTransform = handle.GetComponent<RectTransform>();
            toggleBgImage = toggle.GetComponent<Image>();
            RectTransform handleRect = handle.GetComponent<RectTransform>();
            handleSize = handleRect.sizeDelta.x;
            float toggleSizeX = toggle.sizeDelta.x;
            offPosX = (toggleSizeX / 2) - (handleSize / 2) - handleOffset;
            onPosX = offPosX * -1;
        }
        public void ShowToogle()
        {
            Constructor();
            
            if (isOn)
            {
                toggleBgImage.color = onSpriteBg;
                handleTransform.localPosition = new Vector3(onPosX, handleTransform.localPosition.y, 0f);
            }
            else
            {
                toggleBgImage.color = offSpriteBg;
                handleTransform.localPosition = new Vector3(offPosX, handleTransform.localPosition.y, 0f);
            }
        }

        private void Update()
        {
            if (switching)
            {
                Toggle(isOn);
            }
        }

        public void Switching()
        {
            switching = true;
        }

        public void Toggle(bool toggleStatus)
        {
            if (toggleStatus)
            {
                handleTransform.localPosition = SmoothMove(handle, onPosX, offPosX);
            }
            else
            {
                handleTransform.localPosition = SmoothMove(handle, offPosX, onPosX);
            }
        }

        private Vector3 SmoothMove(GameObject toggleHandle, float startPosX, float endPosX)
        {
            Vector3 position = new Vector3(Mathf.Lerp(startPosX, endPosX, t += speed * 0.05f), 0f, 0f);
            StopSwitching();
            return position;
        }

        private void StopSwitching()
        {
            if (t > 1.0f)
            {
                switching = false;

                t = 0.0f;
                switch (isOn)
                {
                    case true:
                        isOn = false;
                        toggleBgImage.color = offSpriteBg;
                        break;

                    case false:
                        isOn = true;
                        toggleBgImage.color = onSpriteBg;
                        break;
                }
            }
        }
    }
}
