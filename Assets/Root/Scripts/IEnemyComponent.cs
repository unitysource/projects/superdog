using Appside.MonoBehaviours.Enemy;
using UnityEngine;

namespace Appside
{
    public interface IEnemyComponent
    {
        void OnActionEnemy();
        void OnDefeatEnemy(Vector3 dir);
        void Init();
        Collider GetModelCollider();
        Collider GetCollider();

        EnemyType GetEnemyType();
    }
}
