using Appside.MonoBehaviours.Player;
using UnityEngine;

namespace Appside
{
    public interface IPlayerComponent
    {
        void Explosion();
        void UpdateState(StatePlayer statePlayer);
        Collider CheckDamage();
        StatePlayer GetCurrentState();
        void InitBattle(Transform laserHit);
        Transform GetModelTransform();
        Vector3 GetContactPoint();
        void ResetTrigger();
    }
}
