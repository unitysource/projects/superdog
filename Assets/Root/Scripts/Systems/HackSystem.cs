using Appside.Level;
using Appside.MonoBehaviours.MeshSlicing;
using Leopotam.Ecs;
using LeopotamGroup.Globals;

namespace Appside.Systems
{
    sealed class HackSystem : IEcsRunSystem
    {
        private readonly LevelManager _levelManager = default;
        private readonly SliceController _sliceController = default;

        public HackSystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
            _sliceController = Service<SliceController>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (!_levelManager.StateGame.Equals(StateGame.Hack))
                return;
            
            if (!_sliceController.isActiveAndEnabled)
                 _sliceController.enabled = true;
        }
    }
}
