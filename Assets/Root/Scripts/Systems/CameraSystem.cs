using Appside.Level;
using Cinemachine;
using Leopotam.Ecs;
using LeopotamGroup.Globals;

namespace Appside.Systems
{
    sealed class CameraSystem : IEcsRunSystem
    {
        private readonly CinemachineVirtualCamera _gameVirtualCamera = default;
        private readonly CinemachineVirtualCamera _battleVirtualCamera = default;
        private readonly CinemachineVirtualCamera _finishVirtualCamera = default;
        private readonly LevelManager _levelManager = default;
        public CameraSystem(CinemachineVirtualCamera gameVirtualCamera, CinemachineVirtualCamera battleCamera, CinemachineVirtualCamera finishVirtualCamera)
        {
            _gameVirtualCamera = gameVirtualCamera;
            _battleVirtualCamera = battleCamera;
            _finishVirtualCamera = finishVirtualCamera;
            _levelManager = Service<LevelManager>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (_levelManager.StateGame.Equals(StateGame.Battle))
            {
                _gameVirtualCamera.gameObject.SetActive(false);
                _battleVirtualCamera.gameObject.SetActive(true);
                _finishVirtualCamera.gameObject.SetActive(false);
            }

            if (_levelManager.StateGame.Equals(StateGame.Finished) || _levelManager.StateGame.Equals(StateGame.Completed))
            {
                _gameVirtualCamera.gameObject.SetActive(false);
                _battleVirtualCamera.gameObject.SetActive(false);
                _finishVirtualCamera.gameObject.SetActive(true);
            }
        }
    }
}
