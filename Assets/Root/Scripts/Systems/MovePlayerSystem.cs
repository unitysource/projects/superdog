using Appside.Components;
using Appside.MonoBehaviours;
using DG.Tweening;
using Leopotam.Ecs;
using LeopotamGroup.Globals;
using Appside.Level;
using Appside.MonoBehaviours.Player;
using Appside.Utils;
using UnityEngine;

namespace Appside.Systems
{
    sealed class MovePlayerSystem : IEcsRunSystem
    {
        private bool _launchMove;
        private Tween _tween;
        private readonly EcsFilter<Player, PlayerViewComponent> _playerFilter = default;
        private readonly EcsFilter<FailedEvent> _failedEventFilter = default;
        private readonly LevelManager _levelManager = default;
        private readonly GameConfig _gameConfig = default;

        public MovePlayerSystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (_levelManager.StateGame.Equals(StateGame.Start))
            {
                if (!_launchMove)
                {
                    var levelView = Service<LevelView>.Get(false);
                    var path = levelView.GetPath();

                    foreach (var i in _playerFilter)
                    {
                        var player = _playerFilter.Get1(i);
                        var model = _playerFilter.Get2(i).Value.GetModelTransform();

                        _tween = player.Transform.DOPath(path.ToArray(), _gameConfig.DurationMove, PathType.CatmullRom)
                            .OnStart( () => levelView.Init(player.Transform.position.z))
                            .OnComplete(() =>
                            {
                                Service<LevelManager>.Get(false).UpdateState(StateGame.MoveCompleted);
                                _playerFilter.Get2(i).Value.UpdateState(StatePlayer.MoveToFinishPoint);
                                var battlePoint = Service<LevelView>.Get(false).GetBattlePoint;
                                var endPos = new Vector3(battlePoint.position.x, battlePoint.position.y, model.localPosition.z);
                                model.DOLocalMove(endPos, 0.5f).OnComplete(() =>
                                {
                                   Service<LevelManager>.Get(false).UpdateState(StateGame.Battle);
                                   _playerFilter.Get2(i).Value.UpdateState(StatePlayer.Battle);
                                });
                            })
                            .SetEase(Ease.Linear)
                            .SetSpeedBased()
                            .SetLookAt(0.02f);
                        _launchMove = true;
                    }
                }
            }

            if( _failedEventFilter.GetEntitiesCount() > 0)
            {
                if (_tween != null)
                   _tween.Kill();
            }
        }
    }
}
