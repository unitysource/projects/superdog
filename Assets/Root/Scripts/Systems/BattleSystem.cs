using Appside.Level;
using Leopotam.Ecs;
using LeopotamGroup.Globals;
using Appside.Components;
using Appside.MonoBehaviours;
using UnityEngine;
using Appside.Audio;
using Appside.MonoBehaviours.Player;
using Appside.UI;

namespace Appside.Systems
{
    sealed class BattleSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerViewComponent> _playerFilter = default;
        private readonly EcsFilter<BossViewComponent> _bossFilter = default;
        private readonly GameView _gameView = default;
        private LevelManager _levelManager = default;
        private bool _startBattle;
        private bool _stopDelay;
        private Transform _laserPoint;
        private LaserHit _laserHit;
        private float _lastTime;
        private float _interval = 0.5f;
        private float _timeStartBattle;
        private float _timeDelayBattle = 1.5f;
        private int _increment = 0;
        private float _speed = 1f;

        public BattleSystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (!_levelManager.StateGame.Equals(StateGame.Battle))
                return;
            
            if (!_startBattle)
            {
                _startBattle = true;
                _timeStartBattle = Time.realtimeSinceStartup;
                _gameView.Hide();
            }
            
            if (!_stopDelay && Time.realtimeSinceStartup - _timeStartBattle > _timeDelayBattle)
            {
                InitLaser();
                _stopDelay = true;
            }

            if (!_startBattle || !_stopDelay)
                return;
            
            if (Input.GetMouseButtonDown(0))
            { 
                _increment = 1;
                AudioManager.Instance.OnPlaySound(AudioClipId.laser_boss_90);
               _lastTime = Time.realtimeSinceStartup;
            }
            else if (Input.GetMouseButton(0))
            {
                _increment = 1;
            }
            else
            {
                if (Time.realtimeSinceStartup - _lastTime >= _interval)
                    _increment = -1;
            }
            
            UpdatePos();

            var playerZ = _playerFilter.Get1(0).Value.GetModelTransform().position.z;

            if (playerZ + 0.5f >= _laserPoint.position.z)
            {
                _levelManager.UpdateState(StateGame.Finished);
                _playerFilter.Get1(0).Value.UpdateState(StatePlayer.Failed);
                _bossFilter.Get1(0).Value.WinBattle();
            }
            
            if (_laserHit != null) 
            {
                if (_laserHit.HitBoss)
                {
                    _levelManager.UpdateState(StateGame.Hack);
                    _playerFilter.Get1(0).Value.UpdateState(StatePlayer.Hack);
                    _bossFilter.Get1(0).Value.FailBattle();
                }
            }
        }

        void InitLaser()
        {
            _laserPoint = Service<LevelView>.Get(false).GetLaserHit;
            _laserHit = _laserPoint.gameObject.GetComponent<LaserHit>();
                
            foreach (var i in _playerFilter)
            {
                var playerView = _playerFilter.Get1(i).Value;
                playerView.InitBattle(_laserPoint);
            }

            foreach (var i in _bossFilter)
            {
                var bossView = _bossFilter.Get1(i).Value;
                bossView.InitBattle(_laserPoint);
            }

            _lastTime = Time.realtimeSinceStartup;
        }

        private void UpdatePos()
        {
            _laserPoint.position += _increment*new Vector3(0, 0, Time.deltaTime*_speed);
        }
    }
}
