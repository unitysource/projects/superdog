using Leopotam.Ecs;
using LeopotamGroup.Globals;
using Appside.UI;
using Appside.Level;

namespace Appside.Systems
{
    sealed class InitViewSystem : IEcsInitSystem
    {
        private readonly MainMenuView _mainMenuView = default;
        private readonly GameView _gameView = default;
        private readonly SettingsView _settingsView = default;
        private LevelManager _levelManager;
        
        void IEcsInitSystem.Init()
        {
            _levelManager = Service<LevelManager>.Get(false);
            var countScore = _levelManager.ApplicationManager.PlayerAccount.Score;
            _mainMenuView.UpdateScore(countScore);
            _mainMenuView.Show();
            _mainMenuView.StartGameEvent += StartGame;
            _mainMenuView.SettingsShowEvent += ShowSettings;
        }

        void StartGame()
        {
            _mainMenuView.StartGame();
            _gameView.Show(_levelManager.ApplicationManager.PlayerAccount.CurrentLevel);
            _levelManager.UpdateState(StateGame.Start);
        }

        void ShowSettings()
        {
            _settingsView.Show();
        }
    }
}
