using Leopotam.Ecs;
using Appside.Components;
using Appside.MonoBehaviours.Player;
using Appside.UI;
using Appside.Audio;
using Appside.Utils;
using UnityEngine;

namespace Appside.Systems
{
    sealed class UpdateStatePlayerSystem : IEcsRunSystem
    {
        private readonly EcsFilter<LevitationEvent> _levitationFilter = default;
        private readonly EcsFilter<AttackEvent> _attackFilter = default;
        private readonly EcsFilter<FailedEvent> _failedFilter = default;
        private readonly EcsFilter<PlayerViewComponent> _playerViewFilter = default;
        private readonly MainMenuView _mainMenuView = default;
        private readonly GameView _gameView = default;
        private readonly FinishView _finishView = default;
        void IEcsRunSystem.Run()
        {
            foreach (var i in _levitationFilter)
            {
                foreach (var j in _playerViewFilter)
                {
                    var playerView = _playerViewFilter.Get1(j).Value;
                    playerView.UpdateState(StatePlayer.Levitation);
                }
            }
            
            foreach (var i in _attackFilter)
            {
                foreach (var j in _playerViewFilter)
                {
                    var playerView = _playerViewFilter.Get1(j).Value;
                    playerView.UpdateState(StatePlayer.Attack);
                }
            }
            
            foreach (var i  in _failedFilter)
            {
                foreach (var j in _playerViewFilter)
                {
                    var playerView = _playerViewFilter.Get1(j).Value;
                    playerView.UpdateState(StatePlayer.Failed);
                    _gameView.StopScale();
                    _mainMenuView.Hide();
                    _gameView.Hide();
                    _finishView.Show(false);
                }
            }
            
            foreach (var i in _playerViewFilter)
            {
                var playerView = _playerViewFilter.Get1(i).Value;

                if (_gameView.HeatScale.FillAmount <= 0f)
                {
                    playerView.UpdateState(StatePlayer.Levitation);
                    AudioManager.Instance.OnPlaySound(AudioClipId.energy_blast);
                }

                if (playerView.GetCurrentState().Equals(StatePlayer.Attack) && _gameView.HeatScale.CanExplode())
                {
                    playerView.Explosion();
                    var posExplosion = new Vector3( playerView.GetModelTransform().position.x, 0, playerView.GetModelTransform().position.z);
                    ParticleManager.Instance.PlayParticle(ParticleType.ExplosionLanding, posExplosion, true);
                }
            }
        }
    }
}
