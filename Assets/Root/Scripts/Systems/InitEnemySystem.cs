using Appside.Level;
using Appside.Components;
using Leopotam.Ecs;
using LeopotamGroup.Globals;

namespace Appside.Systems
{
    sealed class InitEnemySystem : IEcsRunSystem
    {
        private readonly EcsFilter<EnemyViewComponent> _enemyViewFilter = default;
        private readonly LevelManager _levelManager = default;
        private bool _initEnemy;

        public InitEnemySystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (!_levelManager.StateGame.Equals(StateGame.Start) && _initEnemy)
                return;
            
            _initEnemy = true;
            
            foreach (var i in _enemyViewFilter)
            {
                var view = _enemyViewFilter.Get1(i).Value;
                view.Init();
            }
        }
    }
}
