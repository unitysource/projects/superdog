using Appside.Components;
using Appside.MonoBehaviours;
using Appside.MonoBehaviours.Enemy;
using Leopotam.Ecs;
using UnityEngine;
using Appside.MonoBehaviours.Player;
using Appside.Utils;

namespace Appside.Systems
{
    sealed class InitSystem : IEcsInitSystem
    {
        private EcsWorld _world = default;
        private readonly EcsFilter<PlayerViewComponent> _playerViewFilter = default;
        private readonly EcsFilter<BossViewComponent> _bossViewFilter = default;
        private readonly EcsFilter<EnemyViewComponent> _enemyViewFilter = default;

        void IEcsInitSystem.Init()
        {
            FindPlayer();
            FindBoss();
            FindEnemies();
        }

        void FindPlayer()
        {
            foreach (var unityObject in GameObject.FindGameObjectsWithTag(Tags.Player))
            {
                var tr = unityObject.transform;
                var entity = _world.NewEntity();
                var player = new Player();
                player.Transform = tr;
                entity.Get<PlayerViewComponent>().Value = unityObject.GetComponent<PlayerView>();
                entity.Replace(player);
            }
        }

        void FindBoss()
        {
            foreach (var unityObject in GameObject.FindGameObjectsWithTag(Tags.Boss))
            {
                var entity = _world.NewEntity();
                var boss = new Boss();
                entity.Get<BossViewComponent>().Value = unityObject.GetComponent<BossView>();
                entity.Replace(boss);
            }
        }

        void FindEnemies()
        {
            foreach (var unityObject in GameObject.FindGameObjectsWithTag(Tags.Enemy))
            {
                var entity = _world.NewEntity();
                var enemy = new Enemy();
                entity.Get<EnemyViewComponent>().Value = unityObject.GetComponent<EnemyBase>();
                entity.Replace(enemy);
            }
        }
    }
}
