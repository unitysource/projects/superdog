using Appside.Components;
using Appside.MonoBehaviours.Player;
using Leopotam.Ecs;
using Appside.UI;
using Appside.Level;
using Appside.MonoBehaviours;
using Appside.MonoBehaviours.Enemy;
using LeopotamGroup.Globals;
using Appside.Utils;
using UnityEngine;

namespace Appside.Systems
{
    sealed class AttackSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerViewComponent> _playerViewFilter = default;
        private readonly EcsFilter<EnemyViewComponent> _enemyViewFilter = default;
        private readonly GameView _gameView = default;
        private readonly DamageConfig _damageConfig = default;
        private readonly ShakeConfig _shakeConfig = default;
        private readonly CameraShaker _cameraShaker = default;
        private readonly LevelManager _levelManager = default;
        private readonly EconomConfig _economConfig = default;
        private Collider _collisionObj;

        public AttackSystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
            _cameraShaker = Service<CameraShaker>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (!_levelManager.StateGame.Equals(StateGame.Start))
                return;
            
            foreach (var i in _playerViewFilter)
            {
                var viewPlayer = _playerViewFilter.Get1(i).Value;
                if (viewPlayer.GetCurrentState().Equals(StatePlayer.Attack))
                {
                   
                    _gameView.StartAttack();
                    var index = _shakeConfig.Shake.FindIndex(a => a.Type.Equals(TypeObject.Landing));
                    _cameraShaker.StartAttack(true, _shakeConfig.Shake[index]);
                }

                var reset = false;

                if (viewPlayer.CheckDamage() != null && !viewPlayer.CheckDamage().Equals(_collisionObj))
                {
                    if (viewPlayer.CheckDamage().CompareTag(Tags.ModelEnemy))
                    {
                        foreach (var j in _enemyViewFilter)
                        {
                            var viewEnemy = _enemyViewFilter.Get1(j).Value;

                            if (viewPlayer.CheckDamage().Equals(viewEnemy.GetModelCollider()))
                            {
                                Vector3 dir = viewPlayer.GetContactPoint() - viewPlayer.GetModelTransform().position;
                                dir = -dir.normalized;
                                viewEnemy.OnDefeatEnemy(dir);
                                
                                var index = _damageConfig.DamageObjs.FindIndex(a => a.Type.Equals(TypeObject.Racoon));
                                var damage = _damageConfig.DamageObjs[index].Damage;
                                _gameView.Damage(damage);
                                _collisionObj = viewPlayer.CheckDamage();

                                if (viewEnemy.GetEnemyType().Equals(EnemyType.Enemy_Bag))
                                {
                                    ParticleManager.Instance.PlayParticle(ParticleType.Hit_Racoon_Money, viewPlayer.GetContactPoint());
                                    var score = Random.Range(_economConfig.MinBagWithMoney, _economConfig.MaxBagWithMoney);
                                    _levelManager.ApplicationManager.PlayerAccount.AddedScore(score);
                                }
                                else
                                {
                                    ParticleManager.Instance.PlayParticle(ParticleType.HitRacoon, viewPlayer.GetContactPoint());
                                    _levelManager.ApplicationManager.PlayerAccount.AddedScore(_economConfig.KillEnemy);
                                }

                                var number = _shakeConfig.Shake.FindIndex(a => a.Type.Equals(TypeObject.Racoon));
                                _cameraShaker.ShakeCamera( _shakeConfig.Shake[number]);
                                
                                _gameView.UpdateScore();
                                
                                reset = true;
                            }
                        }
                    }

                    if (viewPlayer.CheckDamage().CompareTag(Tags.Car) && !viewPlayer.CheckDamage().Equals(_collisionObj))
                    {
                        var carCollider = viewPlayer.CheckDamage();
                        var carView = carCollider.GetComponent<CarView>();
                        if (carView != null)
                        {
                            carView.OnDead();
                            var index = _damageConfig.DamageObjs.FindIndex(a => a.Type.Equals(TypeObject.Car));
                            var damage = _damageConfig.DamageObjs[index].Damage;
                            _gameView.Damage(damage);
                            _collisionObj = viewPlayer.CheckDamage();
                            ParticleManager.Instance.PlayParticle(ParticleType.ExplosionFire, carView.transform.position);
                            var number = _shakeConfig.Shake.FindIndex(a => a.Type.Equals(TypeObject.Car));
                            _cameraShaker.ShakeCamera( _shakeConfig.Shake[number]);
                            reset = true;
                        }
                    }
                    
                    if (reset)
                        viewPlayer.ResetTrigger();
                }
            }
        }
    }
}
