using Appside.Components;
using Appside.UI;
using Leopotam.Ecs;
using Appside.MonoBehaviours.Player;
using LeopotamGroup.Globals;
using Appside.Utils;
using UnityEngine;

namespace Appside.Systems
{
    sealed class LevitationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerViewComponent> _playerViewFilter = default;
        private readonly EcsFilter<EnemyViewComponent> _enemyViewFilter = default;
        private readonly GameView _gameView = default;
        private readonly CameraShaker _cameraShaker = default;
        private Collider _collisionObj;

        public LevitationSystem()
        {
            _cameraShaker = Service<CameraShaker>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            foreach (var i in _playerViewFilter)
            {
                var viewPlayer = _playerViewFilter.Get1(i).Value;

                if (viewPlayer.GetCurrentState().Equals(StatePlayer.Levitation))
                {
                    _gameView.StartLevitation();
                    _cameraShaker.StartAttack(false, null);
                }

                if (viewPlayer.CheckDamage() != null && !viewPlayer.CheckDamage().Equals(_collisionObj))
                {
                    if (viewPlayer.CheckDamage().CompareTag(Tags.Enemy))
                    {
                        foreach (var j in _enemyViewFilter)
                        {
                            var viewEnemy = _enemyViewFilter.Get1(j).Value;
                            
                            if (viewPlayer.CheckDamage().Equals(viewEnemy.GetCollider()))
                            {  
                                viewEnemy.OnActionEnemy(); 
                                _collisionObj = viewPlayer.CheckDamage();
                            } 
                        }
                    }
                }
            }
        }
    }
}
