using _Project.Scripts;
using Appside.Level;
using Leopotam.Ecs;
using LeopotamGroup.Globals;
using Appside.Components;
using Appside.MonoBehaviours;
using Appside.MonoBehaviours.Player;
using Appside.UI;
using Appside.Utils;
using UnityEngine;

namespace Appside.Systems
{
    sealed class LevelSystem : IEcsRunSystem
    {
        private readonly LevelManager _levelManager = default;
        private readonly LevelView _levelView = default;
        private readonly GameView _gameView = default;
        private readonly FinishView _finishView = default;
        private readonly EconomConfig _economConfig = default;
        private readonly EcsFilter<Player, PlayerViewComponent> _playerViewFilter = default;
        private readonly EcsFilter<BossViewComponent> _bossFilter = default;
        private EcsWorld _world = default;
        public LevelSystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
            _levelView = Service<LevelView>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (_levelManager.StateGame.Equals(StateGame.Start))
            {
                foreach (var i in _playerViewFilter)
                {
                    var view = _playerViewFilter.Get2(i).Value;
                    var collider = view.CheckDamage();
                    var dist =  _levelView.GethCoveredDistance(_playerViewFilter.Get1(i).Transform.position.z);
                    _gameView.UpdateProgressLevel(dist);

                    if (collider != null)
                    {
                        if (view.GetCurrentState().Equals(StatePlayer.Levitation) && (collider.CompareTag(Tags.Bullet) || collider.CompareTag(Tags.ModelEnemy)))
                        {
                            var entity = _world.NewEntity();
                            var failedEvent = new FailedEvent();
                            entity.Replace(failedEvent);
                            _levelManager.UpdateState(StateGame.Failed);
                        }
                    }
                }
            }

            if (_levelManager.StateGame.Equals(StateGame.Finished))
            {
                AnalyticsManager.GameFailed();
                
                _gameView.Hide();
                _finishView.Show(false);
            }
            if (_levelManager.StateGame.Equals(StateGame.Hack))
            {
                AnalyticsManager.GameCompleted();

                _gameView.Hide();
                _finishView.Show(true, _economConfig.CompleteLevel);
            }
        }
    }
}
