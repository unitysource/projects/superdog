using Leopotam.Ecs;
using Appside.Components;
using Appside.Level;
using LeopotamGroup.Globals;

namespace Appside.Systems
{
    sealed class UpdateStateEnemySystem :  IEcsRunSystem
    {
        private readonly EcsFilter<EnemyViewComponent> _enemyViewFilter = default;
        private readonly LevelManager _levelManager = default;

        public UpdateStateEnemySystem()
        {
            _levelManager = Service<LevelManager>.Get(false);
        }

        void IEcsRunSystem.Run()
        {
            if (!_levelManager.StateGame.Equals(StateGame.Start))
                return;
            
            foreach (var i in _enemyViewFilter)
            {
                var view = _enemyViewFilter.Get1(i).Value;
             //   view.Init();
            }
        }
    }
}
