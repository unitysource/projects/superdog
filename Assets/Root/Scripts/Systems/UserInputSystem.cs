using Leopotam.Ecs;
using UnityEngine;
using Appside.Components;
using Appside.Level;
using LeopotamGroup.Globals;

namespace Appside.Systems
{
    sealed class UserInputSystem : IEcsRunSystem
    {
        private EcsWorld _world = default;
        private bool _press;
        private Vector3 _lastMousePosition;
        
        void IEcsRunSystem.Run()
        {
            if (!Service<LevelManager>.Get(false).StateGame.Equals(StateGame.Start))
                return;
            
            CheckInput();
        }

        void CheckInput()
        {
            if (Input.GetMouseButtonDown(0))
                OnPressEvent();

            if (Input.GetMouseButton(0) && _press)
                OnDragEvent();

            if (Input.GetMouseButtonUp(0))
                OnReleaseEvent();
        }
        
        void OnPressEvent()
        {
            _press = true;
            _lastMousePosition = GetWorldClickPosition();
        }
        
        void OnDragEvent()
        {
            CheckFingerPosition();
        }

        void OnReleaseEvent()
        {
            _press = false;
            
            var entity = _world.NewEntity();
            var levitationEvent = new LevitationEvent();
            entity.Replace(levitationEvent);
        }
        
        void CheckFingerPosition()
        {
            var currentMousePosition = GetWorldClickPosition();
            var levelManager = Service<LevelManager>.Get(false);
            var distX = Vector3.Distance(new Vector3(currentMousePosition.x, 0, 0), new Vector3(_lastMousePosition.x, 0, 0));
            var distY = Vector3.Distance(new Vector3(0, currentMousePosition.y,  0), new Vector3(0, _lastMousePosition.y, 0));
            if ( distY > 0.01f && distX < distY)
            {
                var vect = (currentMousePosition - _lastMousePosition).normalized;
                if (vect.y < 0)
                {
                    var entity = _world.NewEntity();
                    var attackEvent = new AttackEvent();
                    entity.Replace(attackEvent);
                }
                else if (vect.y > 0)
                {
                    var entity = _world.NewEntity();
                    var levitationEvent = new LevitationEvent();
                    entity.Replace(levitationEvent);                  
                }
            }
            else
            {
                var value = levelManager.XDelta + currentMousePosition.x - _lastMousePosition.x;
                levelManager.UpdateXDelta(value);
            }

            _lastMousePosition = currentMousePosition;
        }
        
        Vector3 GetWorldClickPosition()
        {
            var mousePosition = Input.mousePosition;
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 1f));
            worldPoint -= Camera.main.transform.position;
            return Camera.main.transform.InverseTransformDirection(worldPoint);
        }
    }
}
