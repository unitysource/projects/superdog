// using Facebook.Unity;

namespace _Project.Scripts
{
    public static class AnalyticsManager
    {
        private const string Progression = "Level_0";

        public static void GameStarted()
        {
            // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, Progression);
        }

        public static void GameFailed()
        {
            // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, Progression);
        }

        public static void GameCompleted()
        {
            // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Progression);
        }

        public static void GameClosed()
        {
            // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Close, Progression);
        }
        
        public static void GAInitialize()
        {
            // GameAnalytics.Initialize();
        }
        
        public static void FBInitialize()
        {
            // if (FB.IsInitialized)
            //     FB.ActivateApp();
            // else
            //     FB.Init(FB.ActivateApp);
        }
    }
}