using Appside.Save;

namespace Appside.Level
{
    public enum StateGame
    {
        Default,
        Start,
        MoveCompleted,
        Battle,
        Hack,
        Failed,
        Finished,
        Completed
    }
    
    public sealed class LevelManager
    {
        public StateGame StateGame => _stateGame;
        public ApplicationManager ApplicationManager => _applicationManager;
        public float XDelta => _xDelta;

        private float _xDelta;
        private StateGame _stateGame;
        private ApplicationManager _applicationManager;
        
        public LevelManager(ApplicationManager applicationManager)
        {
            _applicationManager = applicationManager;
            _stateGame = StateGame.Default;
        }

        public void UpdateState(StateGame stateGame)
        {
            if (!_stateGame.Equals(stateGame))
                _stateGame = stateGame;
        }

        public void UpdateXDelta(float value)
        {
            _xDelta = value;
        }
    }
}
