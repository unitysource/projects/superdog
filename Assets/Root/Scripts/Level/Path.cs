using UnityEngine;
using System.Collections.Generic;
using Appside.Utils;

namespace Appside.Level
{
    public class Path : MonoBehaviour
    {
        List<Vector3> _path = new List<Vector3>();
        private int curveCount = 0;    
        private int layerOrder = 0;
        private int SEGMENT_COUNT = 50;
        void Awake()
        {
            FindChild();
        }
        void FindChild()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                
                if (child.CompareTag(Tags.Node))
                    _path.Add(child.position);
            }
        }
        public List<Vector3> GetPath()
        {
            return _path;
        }
    }
}
