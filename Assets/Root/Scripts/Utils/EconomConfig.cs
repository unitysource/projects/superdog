using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "EconomConfig", menuName = "EconomConfig", order = 3)]
    public class EconomConfig : ScriptableObject
    {
        public int CompleteLevel;
        public int KillEnemy;
        public float CuttingBoss;
        public int MinBagWithMoney;
        public int MaxBagWithMoney;
        public int UpdateSkills;
    }
}
