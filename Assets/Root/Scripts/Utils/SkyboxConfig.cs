using System;
using System.Collections.Generic;
using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "SkyboxConfig", menuName = "SkyboxConfig", order = 2)]
    public class SkyboxConfig : ScriptableObject
    {
        public List<SkyboxType> SkyboxTypes;
    }

    [Serializable]
    public class SkyboxType
    {
        public int Level;
        public Material Skybox;
    }
}
