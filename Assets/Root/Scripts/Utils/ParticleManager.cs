using System;
using UnityEngine;
using System.Collections.Generic;

namespace Appside.Utils
{
    public enum ParticleType
    {
        None,
        ExplosionFire, 
        HitRacoon, 
        ExplosionLanding,
        Hit_Racoon_Money
    }
    
    [Serializable]
    public class Particle
    {
        public ParticleType ParticleType;
        public ParticleSystem ParticleSystem;
    }
    public class ParticleManager : MonoBehaviour
    {
        public List<Particle> Particles;
        
        public static ParticleManager Instance { get; private set; }
        
        public void PlayParticle(ParticleType particleType, Vector3 position, bool rotate = false, Transform parent = null)
        {
            var particlePrefab = Particles.Find(a => a.ParticleType.Equals(particleType)).ParticleSystem;

            GameObject obj;
            
            if (!rotate) 
                obj = Instantiate(particlePrefab.transform.gameObject, position, Quaternion.identity);
            else 
                obj = Instantiate(particlePrefab.transform.gameObject, position, particlePrefab.transform.rotation);
            
            if (parent != null)
                obj.transform.SetParent(parent);

            var particle = obj.GetComponent<ParticleSystem>();
            float totalDuration = particle.duration + particle.startLifetime; 
            Destroy(obj, totalDuration);
        }
        private void Awake()
        {
            Instance = this;
        }
    }
}
