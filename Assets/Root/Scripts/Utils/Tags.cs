namespace Appside.Utils
{
    public class Tags
    {
        public const string Player = "Player";
        public const string Dog = "Dog";
        public const string Node = "Node";
        public const string Obstacle = "Obstacle";
        public const string Enemy = "Enemy";
        public const string Ground = "Ground";
        public const string Boss = "Boss";
        public const string LaserHit = "LaserHit";
        public const string ModelEnemy = "ModelEnemy";
        public const string Bullet = "Bullet";
        public const string Car = "Car";
        public const string RacoonBag = "RacoonBag";
    }
}
