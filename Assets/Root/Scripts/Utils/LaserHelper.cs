using UnityEngine;

namespace Appside.Utils
{
    public class LaserHelper : MonoBehaviour
    {
        public GameObject HitEffect;
        private ParticleSystem[] Effects;
        private ParticleSystem[] Hit;
        void Start()
        {
            Effects = GetComponentsInChildren<ParticleSystem>();
            Hit = HitEffect.GetComponentsInChildren<ParticleSystem>();
        }

        public void UpdatePosition(Vector3 pos)
        {
            HitEffect.transform.position = pos;
            HitEffect.transform.rotation = Quaternion.identity;
            
            // foreach (var AllPs in Effects)
            // {
            //     if (!AllPs.isPlaying) AllPs.Play();
            // }
        }
    }
}
