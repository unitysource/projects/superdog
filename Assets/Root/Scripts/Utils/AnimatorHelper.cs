using UnityEngine;

namespace Appside.Utils
{
    public class AnimatorHelper : MonoBehaviour
    {
        [SerializeField] private GameObject _skinnedModel;
        [SerializeField] private GameObject _meshModel;

        public void Punch()
        {
            _meshModel.gameObject.SetActive(true);
            _skinnedModel.gameObject.SetActive(false);
        }
    }
}
