using Appside.MonoBehaviours.Enemy;
using UnityEngine;

namespace Appside.Utils
{
    public class ColliderHelper : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag(Tags.Enemy))
            {
                var view = other.gameObject.GetComponent<RobberEnemy>();
                if (view != null)
                   view.StartMove();
            }
        }
    }
}
