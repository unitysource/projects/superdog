using System.Collections.Generic;
using UnityEngine;

namespace Appside.Utils
{
    public class HelpersFunction 
    {
        public static List<GameObject> FindObjectsInRadius(Vector3 pos, float radius, string tag)
        {
            List<GameObject> objects = new List<GameObject>();
            Collider[] colliders = Physics.OverlapSphere(pos, radius);
            foreach (Collider c in colliders)
            {
                if (c.gameObject.CompareTag(tag))
                {
                    objects.Add(c.gameObject);
                }
            }
            
            return objects;
        }
    }
}
