using System.Collections;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "ShakeConfig", menuName = "ShakeConfig", order = 5)]
    public class ShakeConfig : ScriptableObject
    {
        public List<Shake> Shake;
    }
    
    [Serializable]
    public class Shake
    {
        public TypeObject Type;
        public float ShakeDuration;          
        public float ShakeAmplitude;        
        public float ShakeFrequency;    
    }
    
}
