using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "HeatScaleConfig", menuName = "HeatScaleConfig", order = 1)]
    public class HeatScaleConfig : ScriptableObject
    {
        public Color SuperHitColor;
        public Color OverheatColor;
        public Color NormalColor;
        public float SpeedFill;
        
        [Tooltip("Value of overheat up the border from 0.0 to 0.5")] 
        [Range(0.0f, 0.5f)]
        public float OverheatValue;
    }
}
