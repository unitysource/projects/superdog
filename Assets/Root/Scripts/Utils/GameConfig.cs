using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "GameConfig", order = 1)]
    public class GameConfig : ScriptableObject
    {
        [Tooltip("Forward speed player")]
        public float DurationMove;
        
    }
}
