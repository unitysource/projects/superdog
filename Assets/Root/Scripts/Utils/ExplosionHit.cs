using Appside.MonoBehaviours.Enemy;
using UnityEngine;

namespace Appside.Utils
{
    public class ExplosionHit : MonoBehaviour
    {
        [SerializeField] private EnemyBase _enemyBase;

        public void Explosion(Vector3 dir)
        {
            _enemyBase.ExplosionHit(dir);
        }
    }
}
