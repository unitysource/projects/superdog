using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "PlayerConfig", order = 1)]
    public class PlayerConfig: ScriptableObject
    {
        public float HorizontalSpeed;

        public float HorizontalInterpolation;
        
        public float HorizontalBorder;

        public float VerticalSpeed;

        [Tooltip("Force tap player")] 
        public float Forcetap;

        [Tooltip("Max damage when explosion")] 
        public float Damage;
    }
}
