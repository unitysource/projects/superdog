using System;
using System.Collections.Generic;
using UnityEngine;

namespace Appside.Utils
{
    [CreateAssetMenu(fileName = "DamageConfig", menuName = "DamageConfig", order = 5)]
    public class DamageConfig : ScriptableObject
    {
        public List<DamageObj> DamageObjs;
    }

    [Serializable]
    public class DamageObj
    {
        public TypeObject Type;
        public float Damage;
    }

    [Serializable]
    public enum TypeObject
    {
        Car,
        Racoon, 
        Landing
    }
}
