﻿using Appside.Audio;
using Cinemachine;
using UnityEngine;

namespace Appside.Utils
{
    public class CameraShaker : MonoBehaviour
    {
        private float ShakeDuration = 0.3f;          
        private float ShakeAmplitude = 1.2f;        
        private float ShakeFrequency = 2.0f;
        private float ShakeElapsedTime = 0f;
        public CinemachineVirtualCamera VirtualCamera;
        private CinemachineBasicMultiChannelPerlin virtualCameraNoise;

        private bool _currentStateAttack;

        void Start()
        {
            _currentStateAttack = false;
            
            if (VirtualCamera != null)
                virtualCameraNoise = VirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        }

        void Update()
        {
            if (VirtualCamera != null && virtualCameraNoise != null)
            {
                if (ShakeElapsedTime > 0)
                {
                    virtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
                    virtualCameraNoise.m_FrequencyGain = ShakeFrequency;

                    ShakeElapsedTime -= Time.deltaTime;
                }
                else
                {
                    virtualCameraNoise.m_AmplitudeGain = 0f;
                    ShakeElapsedTime = 0f;
                }
            }
        }
        
        public void ShakeCamera(Shake shake)
        {
            ShakeDuration = shake.ShakeDuration;          
            ShakeAmplitude = shake.ShakeAmplitude;
            ShakeFrequency = shake.ShakeFrequency;
            ShakeElapsedTime = ShakeDuration;
        }

        public void StartAttack(bool start, Shake shake)
        {
            if (_currentStateAttack.Equals(start))
                return;

            _currentStateAttack = start;

            if (start)
            {
                ShakeCamera(shake);
                AudioManager.Instance.OnPlaySound(AudioClipId.landing);
            }
        }
    }
}
