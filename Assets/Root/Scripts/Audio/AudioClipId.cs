﻿namespace Appside.Audio
{
    public enum AudioClipId 
    {
        fly_racoon_01,
        fly_racoon_02,
        fly_racoon_03,
        
        hit_racoon_01,
        hit_racoon_02,
        
        energy_blast,
        landing,
        
        hit_racoon_money_01,
        hit_racoon_money_02,
        
        fire_bazooka,
        hit_dog,
        jump_racoon,
        die_dog,
        laser_boss_90,
        
        lasers_01,
        lasers_02,
        
        explosion_car_01,
        explosion_car_02
    }
}
