using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Appside.Audio
{
    public class Sound
    {
        public AudioSource audioSource;
        public AudioClipId clipId;
    }

    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private List<AudioClipEntity> clips;
        [SerializeField] private AudioClip _background;
        private AudioSource _musicSource;
        private List<Sound> _soundsSource = new List<Sound>();
        private Coroutine _coroutine;
        
        public static AudioManager Instance;

        [HideInInspector]
        public List<AudioClipId> _soundFlyRacoon = new List<AudioClipId> {AudioClipId.fly_racoon_01, AudioClipId.fly_racoon_02, AudioClipId.fly_racoon_03};
        
        [HideInInspector]
        public List<AudioClipId> _soundHitRacoon = new List<AudioClipId> {AudioClipId.hit_racoon_01, AudioClipId.hit_racoon_02};
        
        [HideInInspector]
        public List<AudioClipId> _soundHitRacoonMoney = new List<AudioClipId> {AudioClipId.hit_racoon_money_01, AudioClipId.hit_racoon_money_02};
        
        [HideInInspector]
        public List<AudioClipId> _soundExplosionCar = new List<AudioClipId> {AudioClipId.explosion_car_01, AudioClipId.explosion_car_02};
        void Start()
        {
            Instance = this;
            
            SceneManager.sceneLoaded += OnStartGame;

          //  OnPlayMusic();
        }

        private void OnPlayMusic()
        {
            _musicSource =  gameObject.AddComponent<AudioSource>();

            _musicSource.loop = true;
            _musicSource.clip = _background;
            _musicSource.Play();
        }
        
        public void OnPlaySound(List<AudioClipId> audioClipIds)
        {
            var clipId = audioClipIds[Random.Range(0, audioClipIds.Count)];
            var index = clips.FindIndex(a => a.Id == clipId);
            bool isAdd = false;
            AudioSource audioSource = FindAudioSource();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
                isAdd = true;
            }

            audioSource.volume = clips[index].Volume;
            audioSource.clip = clips[index].TargetAudioClip;
            audioSource.Play();
            Sound sound = new Sound();
            sound.audioSource = audioSource;
            sound.clipId = clipId;
            if(isAdd)
                _soundsSource.Add(sound);
            
            StartCoroutine(DeletedClip(audioSource, clips[index].TargetAudioClip.length));
        }

        public void OnPlaySound(AudioClipId clipId)
        {
            var index = clips.FindIndex(a => a.Id == clipId);
            bool isAdd = false;
            AudioSource audioSource = FindAudioSource();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
                isAdd = true;
            }

            audioSource.volume = clips[index].Volume;
            audioSource.clip = clips[index].TargetAudioClip;
            audioSource.Play();
            Sound sound = new Sound();
            sound.audioSource = audioSource;
            sound.clipId = clipId;
            if(isAdd)
                _soundsSource.Add(sound);
            StartCoroutine(DeletedClip(audioSource, clips[index].TargetAudioClip.length));
        }
        
        
        public IEnumerator DeletedClip(AudioSource audioSource, float time)
        {
            yield return new WaitForSeconds(time);
            audioSource.clip = null;
        }

        public AudioSource FindAudioSource()
        {
            AudioSource audio = null;
            foreach (var source in _soundsSource)
            {
                if (source.audioSource.clip == null)
                {
                    audio = source.audioSource;
                }
            }
            return audio;
        }

   /*    public void OnPlayVibration(HapticTypes hapticTypes)
        {
            return;
            if (Service<LevelManager>.Get(false).ApplicationManager.ApplicationSettings.IsVibrationOn)
            { 
                MMVibrationManager.Haptic(hapticTypes); 
                
                #if UNITY_ANDROID
                    MMNVAndroid.AndroidVibrate(20, 25);
                #endif
            }
        }*/
       
        public void OnStartGame(Scene scene, LoadSceneMode mode)
        {
           for (int i = 0; i < _soundsSource.Count; i++)
                 _soundsSource.Remove(_soundsSource[i]);
        }
        
        void Singleton()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
