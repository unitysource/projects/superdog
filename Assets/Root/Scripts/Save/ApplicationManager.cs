using UnityEngine;
using System;
using Appside.Utils;

namespace Appside.Save
{
    public sealed class ApplicationManager 
    {
        private readonly PlayerAccount _playerAccount;
        private readonly ApplicationSettings _applicationSettings;
        
        public const string PLAYER_ACCOUNT_KEY = "REMOTE_ACCOUNT";
        public const string APPLICATION_SETTINGS_KEY = "SETTINGS";
        
        public PlayerAccount PlayerAccount => _playerAccount;

        public ApplicationSettings ApplicationSettings => _applicationSettings;
        
        public ApplicationManager()
        {
            if (PlayerPrefs.HasKey(APPLICATION_SETTINGS_KEY))
            {
                _applicationSettings = JsonSerializer.Deserialize<ApplicationSettings>(PlayerPrefs.GetString(APPLICATION_SETTINGS_KEY));
            }
            else
            {
                _applicationSettings = new ApplicationSettings();
                SaveApplicationSettings();
            }
            
            if (PlayerPrefs.HasKey(PLAYER_ACCOUNT_KEY))
            {
                _playerAccount = JsonSerializer.Deserialize<PlayerAccount>(PlayerPrefs.GetString(PLAYER_ACCOUNT_KEY));
                if (string.IsNullOrEmpty(_playerAccount.PlayerId))
                {
                    _playerAccount.SetPlayerId(Guid.NewGuid().ToString());
                }
            }
            else
            {
                _playerAccount = new PlayerAccount();
                _playerAccount.SetPlayerId(Guid.NewGuid().ToString());
                var remoteData = JsonSerializer.Serialize(_playerAccount);
                PlayerPrefs.SetString(PLAYER_ACCOUNT_KEY, remoteData);
            }
        }
        
        public void SavePlayerAccount()
        {
            var remoteData = JsonSerializer.Serialize(_playerAccount);
            PlayerPrefs.SetString(PLAYER_ACCOUNT_KEY, remoteData);
            PlayerPrefs.Save();
        }
        
        public void SaveApplicationSettings()
        {
            PlayerPrefs.SetString(APPLICATION_SETTINGS_KEY, JsonSerializer.Serialize(_applicationSettings));
        }
    }
}
