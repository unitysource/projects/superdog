using System;

namespace Appside.Save
{
    [Serializable]
    public sealed class PlayerAccount
    {
        public string PlayerId { get; protected set; }
        public int CurrentLevel { get; protected set; }
        public int Score { get; protected set; }

        public PlayerAccount()
        {
            CurrentLevel = 1;
            Score = 0;
        }
        
        public void SetPlayerId(string playerId)
        {
            PlayerId = playerId;
        }

        public void AddedLevel()
        {
            CurrentLevel += 1;
            
            if (CurrentLevel > 11)
                CurrentLevel = 2;
        }
        
        public void AddedScore(int earnedScore)
        {
            Score += earnedScore;
        }
    }
}
