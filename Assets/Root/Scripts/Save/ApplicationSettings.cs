using System;

namespace Appside.Save
{
    [Serializable]
    public sealed class ApplicationSettings
    {
        public bool IsSoundOn = true;
        public bool IsVibrationOn = true;
    }
}
