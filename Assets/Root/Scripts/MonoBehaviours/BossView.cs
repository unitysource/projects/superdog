using System;
using UnityEngine;
using System.Collections.Generic;

namespace Appside.MonoBehaviours
{
    public class BossView : MonoBehaviour, IBossComponent
    {
        [SerializeField] private Laser _laser;
        [SerializeField] private AnimatorManager _animatorManager;
        [SerializeField] private List<Transform> _laserStartPoints;
        [SerializeField] private GameObject _cuttingMesh;
        private Collider _bossCollider;

        void IBossComponent.InitBattle(Transform laserHit)
        {
           _laser.gameObject.SetActive(true); 
           _animatorManager.UpdateTrigger(AnimState.Attack);
           _laser.InitLaser(laserHit, _laserStartPoints);
        }

        Transform IBossComponent.GetTransform()
        {
            return transform;
        }

        void IBossComponent.WinBattle()
        {
            _laser.gameObject.SetActive(false);  
            _animatorManager.UpdateTrigger(AnimState.Win);
        }

        void IBossComponent.FailBattle()
        {
            _laser.gameObject.SetActive(false);  
            _animatorManager.UpdateTrigger(AnimState.Punch);
            _bossCollider.enabled = false;
        }

        private void Start()
        {
            _bossCollider = GetComponent<Collider>();
        }
    }
}
