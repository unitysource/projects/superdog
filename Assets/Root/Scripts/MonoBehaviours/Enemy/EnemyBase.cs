using UnityEngine;
using Appside.Audio;
using Appside.Level;
using LeopotamGroup.Globals;

namespace Appside.MonoBehaviours.Enemy
{
    public enum EnemyType
    {
        Enemy_Default,
        Enemy_Bag
    }
    
    public abstract class EnemyBase : MonoBehaviour, IEnemyComponent
    {
        [SerializeField] protected AnimatorManager _animatorManager;
        [SerializeField] protected Collider _modelCollider;
        [SerializeField] protected Rigidbody _rbModel;
        [SerializeField] protected CharacterRagdoll _characterRagdoll;
        [SerializeField] protected EnemyType _enemyType;
        private Collider _collider;
        private float _force = 10f;
        private float _explosionForce = 10f;

        private void Start()
        {
            _collider = GetComponent<Collider>();
            _animatorManager.RandomIdle();
        }
        public EnemyType GetEnemyType()
        {
            return _enemyType;
        }

        public virtual void OnActionEnemy() { }

        public virtual void OnDefeatEnemy(Vector3 dir)
        {
            if (_collider != null)
               _collider.enabled = false;
            
            if (_characterRagdoll != null)
                _characterRagdoll.EnableRagdollWithForce(_force*new Vector3(Random.Range(-1f, 1f), 0.3f, 1));

            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundFlyRacoon);
        }
        
        public virtual void Init() { }
        
        public virtual Collider GetModelCollider() 
        {
            return null;
        }

        public virtual Collider GetCollider()
        {
            return _collider;
        }

        public virtual void ExplosionHit(Vector3 dir)
        {
            
            if (_collider != null)
                _collider.enabled = false;
            
            if (_characterRagdoll != null)
                _characterRagdoll.EnableRagdollWithForce(_explosionForce*new Vector3(Random.Range(-1f, 1f), 0.3f, 1));

            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundFlyRacoon);
        }
    }
}
