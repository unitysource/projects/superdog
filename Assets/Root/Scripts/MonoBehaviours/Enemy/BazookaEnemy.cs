using System.Collections;
using Appside.Utils;
using Appside.Audio;
using UnityEngine;

namespace Appside.MonoBehaviours.Enemy
{
    public class BazookaEnemy : EnemyBase
    {
        [SerializeField] private Bullet _bullet;
        private bool _fire;
        public override void OnActionEnemy()
        {
            base.OnActionEnemy();

            if (_fire)
                return;

            _animatorManager.UpdateTrigger(AnimState.RaccoonShooting);
            StartCoroutine(StartFire());
        }
        
        public override void OnDefeatEnemy(Vector3 dir)
        {
            base.OnDefeatEnemy(dir);
            
            if (_bullet != null)
                _bullet.gameObject.SetActive(false);
            
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoon);
        }

        public override void ExplosionHit(Vector3 dir)
        {
            base.ExplosionHit(dir);
            
            if (_bullet != null)
                _bullet.gameObject.SetActive(false);
            
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoon);
        }

        public override Collider GetModelCollider()
        {
            return _modelCollider;
        }

        private IEnumerator StartFire()
        {   
            _fire = true;
            yield return new WaitForSeconds(1f);
            _bullet.transform.parent = null;
            
            var dog = GameObject.FindGameObjectWithTag(Tags.Dog);
            
            if (dog != null)
            {
                _fire = true;
                _bullet.transform.parent = null;
                var endPos = dog.transform.position;
                _bullet.Init(endPos);
            }
            else
            {
                Debug.LogWarning("Not find dog");
            }
        }
    }
}
