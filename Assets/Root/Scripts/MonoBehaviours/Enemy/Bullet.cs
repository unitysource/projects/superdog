using UnityEngine;
using Appside.Audio;

namespace Appside.MonoBehaviours.Enemy
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speed = 10f;
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private ParticleSystem _explosion;
        private Vector3 _target;
        private bool _startMove;

        public void Init(Vector3 target)
        {
            _target = target + new Vector3(0, 1, 0);
            _explosion.gameObject.SetActive(true);
            _explosion.Play();
            _particleSystem.gameObject.SetActive(true);
            _particleSystem.Play();
            _startMove = true;
            AudioManager.Instance.OnPlaySound(AudioClipId.fire_bazooka);
        }

        private void Start()
        {
            _particleSystem.gameObject.SetActive(false);
            _explosion.gameObject.SetActive(false);
        }
        
        void Update()
        {
            if (!_startMove)
                return;
            
            float step =  _speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, _target, step);

            if (Vector3.Distance(transform.position, _target) < 0.001f)
            {
                Destroy(gameObject);
            }
        }
    }
}
