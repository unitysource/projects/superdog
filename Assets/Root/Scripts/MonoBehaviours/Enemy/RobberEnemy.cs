using Appside.Level;
using DG.Tweening;
using UnityEngine;
using Appside.Audio;

namespace Appside.MonoBehaviours.Enemy
{
    public class RobberEnemy : EnemyBase
    {
        [SerializeField] private Path _path;
        [SerializeField] private float _speed = 1.5f;
        [SerializeField] private GameObject _bag;
        private Tween _tween;
        private Collider _collider;
        
        private void Start()
        {
            _bag.gameObject.SetActive(false);
            _collider = GetComponent<Collider>();
        }

        public override void OnDefeatEnemy(Vector3 dir)
        {
            base.OnDefeatEnemy(dir);
            _bag.SetActive(false);
            _tween.Kill();
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoonMoney);
        }

        public override void ExplosionHit(Vector3 dir)
        {
            base.ExplosionHit(dir);
            _bag.SetActive(false);
            _tween.Kill();
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoonMoney);
        }

        public override void Init()
        { }

        public void StartMove()
        {
            if (_tween == null)
            {
                if (_collider != null)
                    _collider.enabled = false;
                
                _animatorManager.UpdateTrigger(AnimState.RaccoonStoleBag);
                var path = _path.GetPath();
                _tween = transform.DOPath(path.ToArray(), _speed, PathType.CatmullRom)
                    .OnStart(() =>
                    {
                        GetComponent<Collider>().enabled = false;
                        _animatorManager.UpdateTrigger(AnimState.RaccoonStoleBag);
                        _bag.gameObject.SetActive(true);
                    })
                    .SetEase(Ease.Linear)
                    .SetSpeedBased()
                    .SetLookAt(0.02f);
            }
        }

        public override Collider GetModelCollider()
        {
            return _modelCollider;
        }
    }
}
