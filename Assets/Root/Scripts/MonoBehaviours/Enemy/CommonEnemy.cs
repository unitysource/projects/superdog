using DG.Tweening;
using UnityEngine;
using Appside.Audio;

namespace Appside.MonoBehaviours.Enemy
{
    public class CommonEnemy : EnemyBase
    {
        private Tween _tween;
        public override void OnActionEnemy()
        {
            base.OnActionEnemy();
            _animatorManager.UpdateTrigger(AnimState.AttackInJump);
            AudioManager.Instance.OnPlaySound(AudioClipId.jump_racoon);
            _tween = _modelCollider.transform.DOLocalMoveY(2, 0.5f).OnComplete(() =>
            {
                _modelCollider.transform.DOLocalMoveY(0, 0.5f);
            });
        }

        public override void OnDefeatEnemy(Vector3 dir)
        {
            base.OnDefeatEnemy(dir);
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoon);
        }
        
        public override void ExplosionHit(Vector3 dir)
        {
            base.ExplosionHit(dir);
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundHitRacoon);
        }

        public override Collider GetModelCollider()
        {
            return _modelCollider;
        }
    }
}
