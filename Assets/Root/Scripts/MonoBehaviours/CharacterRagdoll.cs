using UnityEngine;

namespace Appside.MonoBehaviours
{
    public class CharacterRagdoll : MonoBehaviour
    {
        [SerializeField] Rigidbody[] _allRigidbodies;
        [SerializeField] Collider[] _allColliders;
        [SerializeField] Animator _animator;
        [SerializeField] Collider _mainCollider;

        public void ToggleRagdoll(bool state)
        {
            _animator.enabled = !state;

            _mainCollider.isTrigger = state;

            foreach (Rigidbody rb in _allRigidbodies)
            {
                rb.isKinematic = !state;
            }

            foreach (Collider col in _allColliders)
            {
                col.enabled = state;
            }
        }
        public void EnableRagdollWithForce(Vector3 force)
        {
            ToggleRagdoll(true);

            foreach (var rb in _allRigidbodies)
            {
                rb.AddForce(force, ForceMode.Impulse);
            }
        }

        private void Start()
        {
            ToggleRagdoll(false);
        }
    }
}
