using Appside.Utils;
using UnityEngine;

namespace Appside.MonoBehaviours
{
    public class LaserHit : MonoBehaviour
    {
        public bool HitBoss => _hitBoss;

        private bool _hitBoss;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tags.Boss))
            {
                _hitBoss = true;
            }
        }
    }
}
