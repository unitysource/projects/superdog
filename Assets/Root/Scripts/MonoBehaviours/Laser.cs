
using UnityEngine;
using Appside.Audio;
using System.Collections.Generic;
using Appside.Utils;

namespace Appside.MonoBehaviours
{
    public class Laser : MonoBehaviour
    {
        [SerializeField] private List<LineRenderer> _lineRenderers;
        [SerializeField] private List<LaserHelper> _laserHelpers;
        
        private List<Transform> _laserStartPoints;
        private Transform _laserEndPoint;
        private bool _updateLaser;

        public void InitLaser(Transform endPoint, List<Transform> laserStartPoints)
        {
            _laserEndPoint = endPoint;
            _laserStartPoints = laserStartPoints;
            _lineRenderers.ForEach(l =>
            {
                l.gameObject.SetActive(true);
            });
            AudioManager.Instance.OnPlaySound(AudioClipId.lasers_01);
            _updateLaser = true;
        }

        void Update()
        {
            if(!_updateLaser)
                return;
            
            DrawLaser();
        }
        
        void DrawLaser()
        {
            for (var i = 0; i < _lineRenderers.Count; i++)
            {
                _lineRenderers[i].SetPosition(0, _laserStartPoints[i].position);
                var endPos = new Vector3(_laserStartPoints[i].position.x, _laserEndPoint.position.y, _laserEndPoint.position.z);
                _lineRenderers[i].SetPosition(1, endPos);
                _laserHelpers[i].UpdatePosition(endPos);
            }
        }

        private void OnDisable()
        {
            _updateLaser = false;
        }
    }
}
