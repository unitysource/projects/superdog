using UnityEngine;
using Appside.Utils;
using MoreMountains.NiceVibrations;

namespace Appside.MonoBehaviours
{
    public class TriggerHelper : MonoBehaviour
    {
        public Collider TriggerCollider => _triggerCollider;
        public Vector3 ContactPoint => _contactPoint;
        
        Collider _triggerCollider;
        Vector3 _contactPoint;
        
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag(Tags.Enemy) || other.gameObject.CompareTag(Tags.Obstacle) ||
                other.gameObject.CompareTag(Tags.Bullet) || other.gameObject.CompareTag(Tags.Car))
            {
                _triggerCollider = other.collider;
                MMVibrationManager.Haptic(HapticTypes.Selection);
            }
            
            if (other.gameObject.CompareTag(Tags.ModelEnemy))
            {
                _triggerCollider = other.collider;
                _contactPoint = other.contacts[0].point;
                MMVibrationManager.Haptic(HapticTypes.Selection);
            }
        }
        public void Resetrigger()
        {
            if (_triggerCollider != null)
                 _triggerCollider = null;
        }
    }
}
