using System.Collections.Generic;
using _Project.Scripts;
using LeopotamGroup.Globals;
using Appside.Level;
using Appside.Audio;
using Appside.Utils;
using DG.Tweening;
using UnityEngine;

namespace Appside.MonoBehaviours.Player
{
    public enum StatePlayer
    {
        Idle,
        Levitation,
        Attack, 
        MoveToFinishPoint,
        Failed,
        Battle, 
        Hack,
        Run,
    }
    public sealed class PlayerView : MonoBehaviour, IPlayerComponent
    {
        [SerializeField] Transform myTransform;
        [SerializeField] private Laser _laser;
        [SerializeField] private PlayerConfig _playerConfig;
        [SerializeField] private TriggerHelper _triggerHelper;
        [SerializeField] private AnimatorManager _animatorManager;
        [SerializeField] private ParticleSystem _particle;
        [SerializeField] private List<Transform> _laserStartPoints;
        float _targetX;
        Vector3 _localPosition;
        StatePlayer _statePlayer;
        float _endYPoint = 2f;
        float _posY;
        float _direction;
        private bool _endMovement;
        
        private void Start()
        {
            _localPosition = myTransform.localPosition;
            _statePlayer = StatePlayer.Idle;
            _direction = 0;
            _localPosition = myTransform.localPosition;
        }
        
        void ApplyMovement()
        {
            float deltaTime = Time.deltaTime;
            float horizontalBorder = _playerConfig.HorizontalBorder;
            float interpolation = _playerConfig.HorizontalInterpolation;

           _posY = Mathf.Clamp( _posY + _direction*_playerConfig.VerticalSpeed*Time.deltaTime,0, _endYPoint);

           if (_posY.Equals(0))
           {
               _particle.gameObject.SetActive(true);
           }
               
           else
               _particle.gameObject.SetActive(false);

           _targetX += Service<LevelManager>.Get(false).XDelta * deltaTime * _playerConfig.HorizontalSpeed;
            _targetX = Mathf.Clamp(_targetX, -horizontalBorder, horizontalBorder);

            var prevX = _localPosition.x;
            _localPosition.x = Mathf.Lerp(_localPosition.x, _targetX, deltaTime * interpolation);
            
            var movementDelta = _localPosition.x - prevX;

            float rotationFactor = 200;
            float rotationInterp = 15;
            
            var targetRotation = Quaternion.Euler(0, Mathf.Clamp(movementDelta * rotationFactor, -45f, 45f), 0);
            myTransform.localRotation = Quaternion.Slerp(myTransform.localRotation, targetRotation, rotationInterp * deltaTime);

            myTransform.localPosition = new Vector3( _localPosition.x,  _posY, _localPosition.z);

            Service<LevelManager>.Get(false).UpdateXDelta(0);

        }

        void FixedUpdate()
        {
            if (_endMovement)
                return;
            
            ApplyMovement();
        }

        private void Update()
        {
            CheckCollision();
        }

        void CheckCollision()
        {
            // if (_triggerHelper.TriggerCollider != null) 
            //     Debug.Log(_triggerHelper.TriggerCollider.tag);
        }

        Transform IPlayerComponent.GetModelTransform()
        {
            return myTransform;
        }

        Vector3 IPlayerComponent.GetContactPoint()
        {
            return _triggerHelper.ContactPoint;
        }

        void IPlayerComponent.Explosion()
        {
            var enemies = HelpersFunction.FindObjectsInRadius(transform.position, _playerConfig.Damage, Tags.ModelEnemy);

            foreach (var enemy in enemies)
            {
                Vector3 dir = enemy.transform.position - myTransform.position;
                dir = -dir.normalized;
                var explosionHit = enemy.GetComponent<ExplosionHit>();
           //     if (explosionHit != null)
                  //  explosionHit.Explosion(dir);
            }
        }

        void IPlayerComponent.UpdateState(StatePlayer statePlayer)
        {
            if (!_statePlayer.Equals(statePlayer))
            {
                _statePlayer = statePlayer;

                switch (_statePlayer)
                {
                    case StatePlayer.Idle:
                        _animatorManager.UpdateTrigger(AnimState.Idle);
                        break;
                    case StatePlayer.Attack:
                        _animatorManager.UpdateTrigger(AnimState.Run);
                        Attack();
                        break;
                    case StatePlayer.Levitation:
                        _animatorManager.UpdateTrigger(AnimState.Flying);
                        Levitation();
                        break;
                    case StatePlayer.Run:
                        _animatorManager.UpdateTrigger(AnimState.Run);
                        break;
                    case StatePlayer.Failed:
                        _endMovement = true;
                        Failed();
                        break;
                    case StatePlayer.MoveToFinishPoint:
                        _endMovement = true;
                        break;
                    case StatePlayer.Battle:
                        StartBatle();
                        break;
                    case StatePlayer.Hack:
                        Hack();
                        break;
                }
            }
        }
        Collider IPlayerComponent.CheckDamage()
        {
            return _triggerHelper.TriggerCollider;
        }

        void IPlayerComponent.ResetTrigger()
        {
            _triggerHelper.Resetrigger();
        }

        StatePlayer IPlayerComponent.GetCurrentState()
        {
            return _statePlayer;
        }

        void IPlayerComponent.InitBattle(Transform laserHit)
        {
            _laser.gameObject.SetActive(true);
            _laser.InitLaser(laserHit, _laserStartPoints);
        }

        void Levitation()
        {
            _direction = 1f;
        }

        void Attack()
        {
            _direction = -1f;
        }

        void Failed()
        {
            
            AudioManager.Instance.OnPlaySound(AudioClipId.hit_dog);
            _laser.gameObject.SetActive(false);
            _animatorManager.UpdateTrigger(AnimState.Die);
            myTransform.DOMoveY(0, 1f);
            Invoke("PlaySoundDie", 0.5f);
        }

        void PlaySoundDie()
        {
            AudioManager.Instance.OnPlaySound(AudioClipId.die_dog);
        }

        void Hack()
        {
            _laser.gameObject.SetActive(false);
        }

        void StartBatle()
        {
            _animatorManager.UpdateTrigger(AnimState.Battle);
        }
    }
}
