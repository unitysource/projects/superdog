using System.Collections.Generic;
using Appside.Utils;
using Appside.Audio;
using UnityEngine;

namespace Appside.MonoBehaviours
{
    public class CarView : MonoBehaviour
    {
        [SerializeField] private MeshDestroy _meshDestroy;
        [SerializeField] private List<GameObject> _child;
        public void OnDead()
        {
            AudioManager.Instance.OnPlaySound(AudioManager.Instance._soundExplosionCar);
           _meshDestroy.DestroyMesh(0f);
           _child.ForEach(c => c.SetActive(false));
        }
    }
}
