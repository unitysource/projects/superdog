﻿using UnityEngine;

namespace Appside.MonoBehaviours.MeshSlicing
{
	public class SliceableObject : MonoBehaviour
	{
		public bool isConvex = true;
	}
}