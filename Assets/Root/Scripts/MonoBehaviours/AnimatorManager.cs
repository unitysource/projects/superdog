using UnityEngine;

namespace Appside.MonoBehaviours
{
    public enum AnimState
    {
        None,
        Idle,
        Flying, 
        Attack,
        AttackInJump, 
        RaccoonStoleBag,
        RaccoonIdle,
        RaccoonIdleBazuka,
        RaccoonShooting, 
        Die,
        Battle, 
        Win,
        Punch,
        Run
    }

    public class AnimatorManager : MonoBehaviour
    {
        private Animator _animator;
        private AnimState _currentState = AnimState.None;

        private readonly int IdleTrigger = Animator.StringToHash("Idle");
        private readonly int FlyingTrigger = Animator.StringToHash("Flying");
        private readonly int AttackTrigger = Animator.StringToHash("Attack");
        private readonly int AttackInJumpTrigger = Animator.StringToHash("AttackInJump");
        private readonly int RaccoonIdleTrigger = Animator.StringToHash("RaccoonIdle");
        private readonly int RaccoonIdleBazukaTrigger = Animator.StringToHash("RaccoonIdleBazuka");
        private readonly int RaccoonShootingTrigger = Animator.StringToHash("RaccoonShooting");
        private readonly int RaccoonStoleBagTrigger = Animator.StringToHash("RaccoonStoleBag");
        private readonly int DieTrigger = Animator.StringToHash("Die");
        private readonly int BattleTrigger = Animator.StringToHash("Battle");
        private readonly int WinTrigger = Animator.StringToHash("Win");
        private readonly int PunchTrigger = Animator.StringToHash("Punch");
        private readonly int RunTrigger = Animator.StringToHash("Run");

        private void Start()
        {
            _animator = GetComponent<Animator>();
            SetTrigger(AnimState.Idle, IdleTrigger);
        }

        public void RandomIdle()
        {
            GetComponent<Animator>().Play(0,-1, Random.value);
        }

        public void UpdateTrigger(AnimState animState)
        {
            switch (animState)
            {
                case AnimState.Idle:
                    SetTrigger(AnimState.Idle, IdleTrigger);
                    break;
                case AnimState.Flying:
                    SetTrigger(AnimState.Flying, FlyingTrigger);
                    break;
                case AnimState.Attack:
                    SetTrigger(AnimState.Attack, AttackTrigger);
                    break;
                case AnimState.AttackInJump:
                    SetTrigger(AnimState.AttackInJump, AttackInJumpTrigger);
                    break;
                case AnimState.RaccoonStoleBag:
                    SetTrigger(AnimState.RaccoonStoleBag, RaccoonStoleBagTrigger);
                    break;
                case AnimState.RaccoonIdle:
                    SetTrigger(AnimState.RaccoonIdle, RaccoonIdleTrigger);
                    break;
                case AnimState.RaccoonIdleBazuka:
                    SetTrigger(AnimState.RaccoonIdleBazuka, RaccoonIdleBazukaTrigger);
                    break;
                case AnimState.RaccoonShooting:
                    SetTrigger(AnimState.RaccoonShooting, RaccoonShootingTrigger);
                    break;
                case AnimState.Die:
                    SetTrigger(AnimState.Die, DieTrigger);
                    break;
                case AnimState.Battle:
                    SetTrigger(AnimState.Battle, BattleTrigger);
                    break;
                case AnimState.Win:
                    SetTrigger(AnimState.Win, WinTrigger);
                    break;
                case AnimState.Punch:
                    SetTrigger(AnimState.Punch, PunchTrigger);
                    break;
                case AnimState.Run:
                    SetTrigger(AnimState.Run, RunTrigger);
                    break;
            }
        }

        private void SetTrigger(AnimState animState, int state)
        {
            if (_currentState.Equals(animState))
                return;
            
            _animator.SetTrigger(state);
            _currentState = animState;
        }
    }
}
