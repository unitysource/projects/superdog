using System.Collections.Generic;
using UnityEngine;
using Appside.Level;

namespace Appside.MonoBehaviours
{
    public class LevelView : MonoBehaviour
    {
        [SerializeField] private Path _levelPath;
        [SerializeField] private Transform _laserHit;
        [SerializeField] private Transform _battlePoint;
        private float _startPosition;
        public Transform GetLaserHit => _laserHit;
        public Transform GetBattlePoint => _battlePoint; 

        public List<Vector3> GetPath()
        {
            var partPath = _levelPath.GetPath();
            return partPath;
        }
        public float GethCoveredDistance(float currentPos)
        {
            var path = _levelPath.GetPath();
            var endPos = path[path.Count - 1].z;
            var coveredDist = 1 - (endPos - currentPos) / (endPos - _startPosition);
            return coveredDist;
        }
        public void Init(float startPos)
        {
            _startPosition = startPos;
        }
    }
}
